package com.example.usuario.lista;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class WebViewActivity extends AppCompatActivity implements View.OnClickListener {

    private WebView navegador;
    private String direccion;
    private Button botonAtras;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        }

        direccion = getIntent().getStringExtra("direccion");

        navegador = (WebView) findViewById(R.id.webview);
        botonAtras = (Button) findViewById(R.id.button);

        //habilitamos el zoom y javascript
        navegador.getSettings().setJavaScriptEnabled(true);
        navegador.getSettings().setBuiltInZoomControls(true);

        navegador.canGoBack();

        navegador.loadUrl(direccion);

        navegador.setWebViewClient(new WebViewClient()
        {
            // evita que los enlaces se abran fuera nuestra app en el navegador de android
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                return false;
            }

        });

        botonAtras.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        navegador.goBack();
    }
}
